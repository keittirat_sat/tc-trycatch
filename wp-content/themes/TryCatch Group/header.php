<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/img/favicon_trycatch.ico" type="image/x-icon" />

        <title>
            <?php global $page, $paged; ?>
            <?php wp_title('|', true, 'right'); ?>
            <?php bloginfo('name'); ?>
            <?php $site_description = get_bloginfo('description', 'display'); ?>
            <?php if ($site_description && ( is_home() || is_front_page() )) echo " | $site_description"; ?>
            <?php if ($paged >= 2 || $page >= 2) echo ' | ' . sprintf(__('Page %s', 'twentyeleven'), max($paged, $page)); ?>
        </title>

        <link href='<?php bloginfo('template_directory'); ?>/css/wp_reset.css' rel='stylesheet' type='text/css'>
        <link href='<?php bloginfo('template_directory'); ?>/css/960.css' rel='stylesheet' type='text/css'>
        <link href='<?php bloginfo('template_directory'); ?>/css/bootstrap.min.css' rel='stylesheet' type='text/css'>
        <link href='<?php bloginfo('template_directory'); ?>/font/rsu.css' rel='stylesheet' type='text/css'>
        <link href='<?php bloginfo('template_directory'); ?>/css/colorbox.css' rel='stylesheet' type='text/css'>
        <link href='<?php bloginfo('template_directory'); ?>/css/nivo-slider.css' rel='stylesheet' type='text/css'>
        <!--<link href='<?php bloginfo('template_directory'); ?>/css/jquery.mCustomScrollbar.css' rel='stylesheet' type='text/css'>-->
        <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />

        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery-1.7.min.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/bootstrap.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.metadata.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.colorbox-min.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.nivo.slider.pack.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/cufon-yui.js"></script>
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/Thai_Sans_Lite_400.font.js"></script>
        <?php if (is_page(13)): ?>
            <script src="http://maps.google.com/maps/api/js?sensor=true" type="text/javascript"></script>
            <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery-ui-1.8.18.min.js"></script>
            <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.ui.map.js"></script> 
        <?php endif; ?>
<!--<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.mCustomScrollbar.js"></script>--> 
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.trycatch.js"></script>


        <?php
        /* We add some JavaScript to pages with the comment form
         * to support sites with threaded comments (when in use).
         */
        if (is_singular() && get_option('thread_comments'))
            wp_enqueue_script('comment-reply');

        /* Always have wp_head() just before the closing </head>
         * tag of your theme, or you will break many plugins, which
         * generally use this hook to add elements to <head> such
         * as styles, scripts, and meta tags.
         */
        wp_head();
        ?>

    </head>
    <body <?php body_class(); ?>>
        <!-- main menu -->
        <div class="bg_menu">
            <div class="top_menu">
                <div class="container_12 clearfix">
                    <a href="/" class="logo_menu"></a>
                    <div class="switch-lang-box">
                        <a href="#lang" class="bg-lang-flag">
                            <div class="lang-toggle <?php echo qtrans_getLanguage(); ?>"></div>
                        </a>
                    </div>
                    <?php $menu = array_reverse(get_plain_menu()); ?>
                    <?php foreach ($menu as $key => $top_menu): ?>
                        <?php $active = ""; ?>
                        <?php switch ($key): case 3: ?>
                                <?php if (is_home()): ?>
                                    <?php $active = "active"; ?>
                                <?php endif; ?>
                                <?php break; ?>
                            <?php case 0: ?>
                                <?php if (is_page('contact-us')): ?>
                                    <?php $active = "active"; ?>
                                <?php endif; ?>
                                <?php break; ?>
                            <?php case 2: ?>
                                <?php if (is_page('about-us')): ?>
                                    <?php $active = "active"; ?>
                                <?php endif; ?>
                                <?php break; ?>
                            <?php case 1: ?>
                                <?php if (is_page('portfolio') || is_category('desktop-application') || is_category('graphic-and-logo-design') || is_category('mobile-application') || is_category('network-solution') || is_category('website')): ?>
                                    <?php $active = "active"; ?>
                                <?php endif; ?>
                                <?php break; ?>
                        <?php endswitch; ?>
                        <a href="<?php echo $top_menu['url']; ?>" class="primary-menu <?php echo $active; ?>">
                            <span class="menu_order menu_order<?php echo $key; ?>"></span>
                            <span><?php echo $top_menu['title']; ?></span>
                        </a>
                    <?php endforeach; ?>
                </div>
            </div>
        </div><!-- end main menu -->