<?php get_header(); ?>
<div class="mini-slide-freeform">
    <div class="container_12" style="position: relative; height: 150px;">
        <h1 class="cate_title "><?php wp_title("", true, 'right'); ?></h1>
    </div>
</div>
<div class="container_12 main-content">
    <div class="grid_9">
        <!--Category Bar-->
        <h2 class="textblue subtitle-forcate ">Category</h2>
        <p class="cate-bar">
            <?php $cate = get_all_category_ids(); ?>
            <?php $index = 0; ?>
            <a href="<?php echo get_page_link(18) ?>" class="other_cate_link"><?php echo (qtrans_getLanguage() == "th") ? "ผลงานทั้งหมด" : "View all"; ?></a>
            <?php foreach ($cate as $rec): ?>
                <?php if ($rec != 1): ?>
                    <?php $c = get_category($rec); ?>
                    <?php $c->link = get_category_link($rec); ?>
                    <font class='textblue'><b> // </b></font>
                    <a href="<?php echo $c->link ?>" class="other_cate_link"><?php echo $c->name; ?></a>
                    <?php $index++; ?>
                <?php endif; ?>
            <?php endforeach; ?>
        </p><!--End Category Bar-->

        <?php while (have_posts()) : the_post(); ?>
            <div class="each-portfolio clearfix">
                <div class="grid_6 begin portfolio-thumbail">
                    <?php echo get_the_post_thumbnail(); ?>
                </div>
                <div class="grid_3 end">
                    <a href='<?php the_permalink(); ?>'><h2 class="" style="margin-top: 0px;"><?php echo the_title(); ?></h2></a>
                    <p><?php the_excerpt(); ?></p>
                    <?php $url = get_post_custom_values('url'); ?>
                    <?php if (count($url)): ?>
                        <p>
                            <a href="<?php echo $url[0]; ?>" target="_blank" rel="nofollow"><i class="icon-trycatch cate-link"></i>Go to Client's site</a>
                        </p>
                    <?php endif; ?>
                    <p><?php the_content(); ?></p>
                    <p class='clearfix'>
                        <?php $image = get_all_post_image(get_the_ID(), 'post-thumbnails'); ?>
                        <?php foreach ($image as $img): ?>
                            <a href="<?php echo $img ?>" style="background-image:url('<?php echo $img ?>');" class="grid_1 begin portfolio-other-thumb colorbox" rel="port_<?php echo get_the_ID(); ?>" title="<?php echo get_the_title(); ?>"></a>
                        <?php endforeach; ?>
                    </p>
                </div>
            </div>
        <?php endwhile; ?>

    </div>
    <?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>