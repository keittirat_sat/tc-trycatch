<?php get_header(); ?>
<?php while (have_posts()): the_post(); ?>
    <div class="mini-slide-freeform">
        <div class="container_12" style="position: relative; height: 150px;">
            <h1 class="cate_title ">
                <?php switch (get_the_ID()): case 18 : ?>
                        <font class="textblue">Port</font>folio.
                        <?php break; ?>
                    <?php case 13: ?>
                        <font class="textblue">Contact</font> Us.
                        <?php break; ?>
                    <?php case 12: ?>
                        <font class="textblue">About</font> Us.
                        <?php break; ?>
                    <?php default : ?>
                        <?php echo get_the_ID(); ?>
                        <?php break; ?>
                <?php endswitch; ?>
            </h1>
        </div>
    </div>
    <div class="container_12 main-content">
        <div class="grid_9">    
            <?php if (get_the_ID() == 18): ?>
                <!--Category Bar-->
                <h2 class="textblue subtitle-forcate ">Category</h2>
                <p class="cate-bar">
                    <?php $cate = get_all_category_ids(); ?>
                    <?php $index = 0; ?>
                    <a href="<?php echo get_page_link(18) ?>" class="other_cate_link"><?php echo (qtrans_getLanguage() == "th") ? "ผลงานทั้งหมด" : "View all"; ?></a>
                    <?php foreach ($cate as $rec): ?>
                        <?php if ($rec != 1): ?>
                            <?php $c = get_category($rec); ?>
                            <?php $c->link = get_category_link($rec); ?>
                            <font class='textblue'><b> // </b></font>
                            <a href="<?php echo $c->link ?>" class="other_cate_link"><?php echo $c->name; ?></a>
                            <?php $index++; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </p><!--End Category Bar-->
            <?php endif; ?>

            <?php switch (get_the_ID()): case 18: ?>
                    <!--Portfolio Page-->
                    <?php $myposts = get_posts(array('orderby' => 'ID', 'order' => 'desc', 'posts_per_page' => 1000)); ?>
                    <?php foreach ($myposts as $post) : setup_postdata($post); ?>
                        <div class="each-portfolio clearfix">
                            <div class="grid_6 begin portfolio-thumbail">
                                <?php echo get_the_post_thumbnail(); ?>
                            </div>
                            <div class="grid_3 end">
                                <a href='<?php the_permalink(); ?>'><h2 class="" style="margin-top: 0px;"><?php echo the_title(); ?></h2></a>
                                <p><?php the_excerpt(); ?></p>
                                <?php $url = get_post_custom_values('url'); ?>
                                <?php if (count($url)): ?>
                                    <p>
                                        <a href="<?php echo $url[0]; ?>" target="_blank" rel="nofollow"><i class="icon-trycatch cate-link"></i>Go to Client's site</a>
                                    </p>
                                <?php endif; ?>
                                <p><?php the_content(); ?></p>
                                <p class='clearfix'>
                                    <?php $image = get_all_post_image(get_the_ID(), 'post-thumbnails'); ?>
                                    <?php foreach ($image as $img): ?>
                                        <a href="<?php echo $img ?>" style="background-image:url('<?php echo $img ?>');" class="grid_1 begin portfolio-other-thumb colorbox" rel="port_<?php echo get_the_ID(); ?>" title="<?php echo get_the_title(); ?>"></a>
                                    <?php endforeach; ?>
                                </p>
                            </div>
                        </div>
                    <?php endforeach; ?>

                    <?php break; ?>
                <?php case 13: ?>
                    <!--Contact Us-->                    
                    <div id="map"></div>
                    <div class="clearfix">
                        <div class="grid_4 begin contact-detail">
                            <h1 class="textblue" style="margin-top:0px; font-weight: normal;">TryCatch&trade;</h1>
                            <p style="height: 75px;">
                                <i class="icon-trycatch blue pin"></i>
                                <span>Address<br/>Trycatch 52/13 2<sup>nd</sup> English Balloon building Singharat Rd. T.Sriphum A.Muang Chiangmai<br/>Thailand 50200</span>
                            </p>
                            <p style="height: 25px;">
                                <i class="icon-trycatch blue tel"></i>
                                <span>+66 83 323 9475<br/>+66 89 700 2217</span>
                            </p>
                            <p>
                                <i class="icon-trycatch blue email"></i>
                                <span style="top:2px;">
                                    <a href="mailto:keittirat@trycatch.in.th">keittirat@trycatch.in.th</a>, 
                                    <a href="mailto:apichai@trycatch.in.th">apichai@trycatch.in.th</a>
                                </span>
                            </p>
                        </div>
                        <div class="grid_5 end">
                            <?php if ($_POST): ?>
                                <?php @gdi_mail($_POST); ?>
                                <div class="alert alert-success">
                                    <strong>Thank you</strong><br>
                                    We will contact you back shortly. ;-)
                                </div>
                            <?php endif; ?>
                            <form class="contact-form" method="post">
                                <label class="textblue">Name<br/><input type="text" name="contact-name"></label>
                                <label class="textblue">E-mail<br/><input type="text" name="contact-mail"></label>
                                <label class="textblue">Message<br/><textarea name="contact-detail"></textarea></label>
                                <span class="clearfix">
                                    <button style="float: right;" type="reset" class="btn btn-danger">Reset</button>
                                    <button style="float: right; margin-right: 5px;" type="submit" class="btn btn-primary">Submit</button>
                                </span>
                                <input type="hidden" value="http://<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]; ?>" id="destination">
                            </form>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $(function() {
                            $('#map').gmap({
                                'mapTypeId': google.maps.MapTypeId.HYBRID,
                                'zoom': 15,
                                'center': '18.792221,98.982038'
                            }).bind('init', function(ev, map) {
                                $('#map').gmap('addMarker', {'position': '18.792221,98.982038', 'icon': '<?php bloginfo('template_directory'); ?>/img/mappin.png'}).click(function() {
                                    var marker = "<div style='height:150px;'>"
                                    marker += "<img src='<?php bloginfo('template_directory'); ?>/img/splashlogo.png' width='100' style='margin-bottom:5px; display:block;' class='bb-box'><br>";
                                    marker += "<b>Call</b>: 089-700-2217, 083-323-9475<br/>";
                                    marker += "<b>Email</b>: keittirat@trycatch.in.th";
                                    marker += "</div>";
                                    $('#map').gmap('openInfoWindow', {'content': marker}, this);
                                });
                            });
                        });
                    </script>
                    <?php break; ?>
                <?php case 12: ?>
                    <!--About Us-->
                    <p style="text-align: center; margin: 20px 0px;"><img src="<?php bloginfo('template_directory'); ?>/img/splashlogo.png" title="TryCatch"></p>
                    <p style="margin: 15px 0px;"><?php the_content(); ?></p>
                    <?php break; ?>
            <?php endswitch; ?>
            &nbsp;
        </div>
        <?php get_sidebar(); ?>
    </div>
<?php endwhile; ?>
<?php get_footer(); ?>
