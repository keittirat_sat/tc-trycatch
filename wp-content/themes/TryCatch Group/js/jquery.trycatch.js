/*
 *  Javascript for CMR
 *  Dev by TryCatch
 *  THIS FILE FOR GLOBAL JQS FUNCTION
 **/
$(function() {
    Cufon.replace('.cufon');
    $('a.primary-menu').mouseenter(function() {
        if (!$(this).is('.active')) {
            $(this).find('span.menu_order').css({'background-position-y': '-63px'}).stop().animate({'margin-top': 15}, 300);
        }
    }).mouseleave(function() {
        if (!$(this).is('.active')) {
            $(this).find('span.menu_order').css({'background-position-y': '-113px'}).stop().animate({'margin-top': 20}, 300);
        }
    });

    $('.accordion-group a').mouseenter(function() {
        $(this).find('.icon-trycatch').stop().animate({'margin-right': '10'}, 300);
    }).mouseleave(function() {
        $(this).find('.icon-trycatch').stop().animate({'margin-right': '5'}, 300);
    });

    $('a.colorbox').colorbox({'maxWidth': '90%', 'maxHeight': '90%'});

    

    $('a.bg-lang-flag').click(function() {
        var link;
        if ($('.lang-toggle').is('.en')) {
            $('.lang-toggle').animate({top: '2px'}, 300, function() {
                link = $('a.qtrans_flag_th').attr('href');
                location.href = link;
            });
        } else {
            $('.lang-toggle').animate({top: '30px'}, 300, function() {
                link = $('a.qtrans_flag_en').attr('href');
                location.href = link;
            });
        }
        return false;
    });
});