<?php get_header(); ?>

<!-- slide and promotion pinned -->
<div class="slide-freeform">
    <div class="promotion"></div>
</div><!-- end slide and promotion pinned -->

<!-- start main content -->
<div class="container_12 main-content">
    <p class="proem">
        <span>We craft <font class='textblue'>your</font> dreams.<br/>Simple, Luxury and <font class='textblue'>Professional</font></span>
    </p>

    <div class="grid_9">
        <!--Slide-->
        <div class="slide-index clearfix">
            <div class="frame-connect-us">
                <!--Ads slide-->
                <div class="connect-us">
                    <div class="grid_6 begin ">
                        <h2><font class="textblue">Flexible</font> Layout</h2>
                        <p class="txt_index">
                            <?php if (qtrans_getLanguage() == "th"): ?>
                                สามารถกำหนดรูปแบบ ความโดดเด่นของเว็บไซต์ ให้ตอบสนองต่อรูปลักษณ์ขององค์กร และ function การทำงาน ให้ครบคุณสมบัติและใช้งานได้ง่ายตาม รูปแบบของ UX และ UI
                            <?php else: ?>
                                We can specify a distinct format for the website that can gel with your organization’s identity and complete work functions through all the qualities of UX and UI formating.
                            <?php endif; ?>
                        </p>
                    </div>

                    <div class="grid_3 end">
                        <div class="grid_3_mockup"><img src="<?php bloginfo('template_directory'); ?>/img/top_layout.png" alt="Flexible Layout" ></div>
                    </div>
                </div>
                <div class="connect-us">
                    <div class="grid_6 begin">
                        <h2>Design with  <font class="textblue">Professional</font></h2>
                        <p class="txt_index">                            
                            <?php if (qtrans_getLanguage() == "th"): ?>
                                กำหนดสี และโทนของรูปลักษณ์ องค์กรที่ต้องการจะสื่อออกมาให้กับผู้เข้าชมได้สัมผัส ถึงอัตลักษณ์ที่แฝงอยู๋ในชิ้นงาน และทำให้ชิ้นงานมีความเฉพาะเจาะจง ไม่ซ้ำกับที่อื่น ง่ายต่อการจดจำ
                            <?php else: ?>
                                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
                            <?php endif; ?>
                        </p>
                    </div>

                    <div class="grid_3 end">
                        <div class="grid_3_mockup"><img src="<?php bloginfo('template_directory'); ?>/img/top_design.png" alt="Design with Professional" ></div>
                    </div>
                </div>
                <div class="connect-us">
                    <div class="grid_6 begin">
                        <h2><font class="textblue">Great</font> Support</h2>
                        <p class="txt_index">
                            <?php if (qtrans_getLanguage() == "th"): ?>
                                การันตีชิ้นงานด้วยการรับประกันข้อผิดพลาดที่เกิดจากระบบถึง 1 ปีเต็ม เพื่อให้ลูกค้าสบายใจ เมื่อใช้บริการกับเรา ว่าเราจะไม่ทิ้งชิ้นงานของลูกค้าให้ลอยแพ
                            <?php else: ?>
                                Trusting our service means we will guarantee against any system failure for one full year, helping the customer feel comfortable and secure,and we will never abandon the client.
                            <?php endif; ?>
                        </p>
                    </div>

                    <div class="grid_3 end">
                        <div class="grid_3_mockup"><img src="<?php bloginfo('template_directory'); ?>/img/top_support.png" alt="Great Support" ></div>
                    </div>

                </div>
                <div class="connect-us">
                    <div class="grid_6 begin">
                        <h2><font class="textblue">Reasonable</font> Price</h2>
                        <p class="txt_index">                            
                            <?php if (qtrans_getLanguage() == "th"): ?>
                                คุณภาพของชิ้นงาน สมกับราคาที่ท่านจ่าย และมีการรับประกันชิ้นงานทุกชิ้นเป็นเวลา 1 ปีเต็ม และการบริการระบบ Hosting ที่เราคัดเลือกมาให้ท่านที่รับประกัน Uptime เพื่อไม่ให้ท่านพลาดโอกาสทางธุรกิจ
                            <?php else: ?>
                                Create a quality  portfolio well worth the price you pay that also has a total guarantee for all your work. For one full year with service by the host that we choose for you. Don’t miss this oppornity in business ! 
                            <?php endif; ?>
                        </p>
                    </div>

                    <div class="grid_3 end">
                        <div class="grid_3_mockup"><img src="<?php bloginfo('template_directory'); ?>/img/top_price.png" alt="Reasonable Price" ></div>
                    </div>
                </div>
                <div class="connect-us">
                    <div class="grid_6 begin">
                        <h2>Great <font class="textblue">Usability</font></h2>
                        <p class="txt_index">
                            <?php if (qtrans_getLanguage() == "th"): ?>
                                การออกแบบงาน เน้นการใช้งานที่ง่าย และครบทุก function ที่กำหนด โดยใช้มาตรฐานการออกแบบของ UX และ UI ทำให้ชิ้นงานออกมาสามารถตอบสนองต่อการใช้งานของลูกค้า
                            <?php else: ?>
                                Our design works forms emphasize ease of use and come complete with all functions specified by the standards of UX and UI . They can respond to what customers want.
                            <?php endif; ?>
                        </p>
                    </div>

                    <div class="grid_3 end">
                        <div class="grid_3_mockup"><img src="<?php bloginfo('template_directory'); ?>/img/top_usability.png" alt="Great Usability" ></div>
                    </div>
                </div>
                <!--End Ads slide-->
            </div>
        </div><!--End Slide-->
        <!--According Bar-->
        <h2 class='our-skill'><font class='textblue'>Our</font> Skill</h2>
        <div class="accordion" id="accordion2">
            <div class="accordion-group" style='background-color: #fd9834;'>
                <div class="accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse1">
                        <i class='icon-trycatch world'></i>Website with responsive UI
                    </a>
                </div>
                <div id="collapse1" class="accordion-body collapse in">
                    <div class="accordion-inner clearfix">
                        <div class="grid_3 begin">
                            <div class="grid_3_mockup"><img src="<?php bloginfo('template_directory'); ?>/img/bot_responsive_2.png" alt="Website with responsive" ></div>
                        </div>
                        <div class="detail-box">      
                            <?php if (qtrans_getLanguage() == "th"): ?>
                                <p><a href="<?php echo home_url(); ?>">TryCatch&trade;</a> เราสามารถสร้างเว็บไซต์ ที่ตอบสนองกับความต้องการขององค์กร พร้อมนำเสนอเทคโนโลยีใหม่ล่าสุดที่สามารถทำงานได้ทุกขนาดจอภาพของอุปกรณ์ (มือถือ, แท็บเลต, คอมพิวเตอร์)</p>
                                <p>เราสามารถทำเว็บไซต์ที่มีการทำงานเฉพาะทาง เช่นการประมวลผล Transaction การประมวลผล Log ขององค์กร หรือระบบ การเชื่อมต่อ บริการของทาง Facebook Twitter etc...</p>
                            <?php else: ?>
                                <p>Our TrycatchTm  team can create websites that respond to the needs of every organization and present you with the lastest technologies that can work with all of your instruments (iPhone,Smartphone,iPad,Tablet,PC,etc...)</p>
                                <p>We can make special websites for specific projects such as processing transactions , logging processes of organizations,Systems that can link with social networks ,etc…</p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion-group" style='background-color: #6599cb;'>
                <div class="accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse2">
                        <i class='icon-trycatch brush'></i>Graphic and Logo Design
                    </a>
                </div>
                <div id="collapse2" class="accordion-body collapse">
                    <div class="accordion-inner clearfix">                        
                        <div class="grid_3 begin">
                            <div class="grid_3_mockup"><img src="<?php bloginfo('template_directory'); ?>/img/bot_design_2.png" alt="Graphic and logo design" ></div>
                        </div>
                        <div class="detail-box">                                 
                            <?php if (qtrans_getLanguage() == "th"): ?>
                                <p>ต้องการอะไรที่โดดเด่นใช่ไหม ?</p>
                                <p>ทางทีมงาน <a href="<?php echo home_url(); ?>">TryCatch&trade;</a> เรามีทีมดีไซน์ที่ สามารถทำให้ องค์กรของท่านมีความโดดเด่น และมีเอกลักษณ์เฉพาะตัว ที่เป็นที่น่าจดจำแก่ผู้พบเห็น</p>
                                <ul>
                                    <li>โลโก้</li>
                                    <li>ซองจดหมาย</li>
                                    <li>หัวกระดาษ</li>
                                    <li>แฟ้มใส่เอกสาร</li>
                                    <li>ลายเสื้อ</li>
                                    <li>ป้ายโฆษณา</li>
                                    <li>อื่นๆ</li>
                                </ul>
                            <?php else: ?>
                                <p>Want something that stands out?</p>
                                <p>Our TrycatchTM team has a designer team that can make your organization have own identities can make people easy remember.</p>
                                <ul>
                                    <li>Logos</li>
                                    <li>Envelopes</li>
                                    <li>Heads letter</li>
                                    <li>Bags</li>
                                    <li>Shirts</li>
                                    <li>Avertising Signs</li>
                                    <li>etc.</li>
                                </ul>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion-group" style='background-color: #cc6566;'>
                <div class="accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse3">
                        <i class='icon-trycatch cube'></i>Desktop Applications
                    </a>
                </div>
                <div id="collapse3" class="accordion-body collapse">
                    <div class="accordion-inner clearfix">
                        <div class="grid_3 begin">
                            <div class="grid_3_mockup"><img src="<?php bloginfo('template_directory'); ?>/img/bot_desktop_2.png" alt="Desktop Application" ></div>
                        </div>
                        <div class="detail-box"> 
                            <?php if (qtrans_getLanguage() == "th"): ?>       
                                <p>ต้องการโปรแกรมทำงานเฉพาะด้าน ?</p>
                                <p>เรา <a href="<?php echo home_url(); ?>">TryCatch&trade;</a> สามารถสร้างชุดโปรแกรมที่สามารถทำงานได้เฉพาะทาง และช่วยให้คุณทำงานได้เร็วมากขึ้น โดยเราสามารถทำโปรแกรมที่ใช้ในการประมวล</p>
                                <ul>
                                    <li>สถิติ</li>
                                    <li>การจัดการรายงาน</li>
                                    <li>การประมวลผลข้อมูล Transaction</li>
                                    <li>การทำงานเชื่อมต่อกับระบบ Server บริการข้อมูล</li>
                                </ul>
                            <?php else: ?>
                                <p>Want a specific program for specific work ?</p>
                                <p>Our TrycatchTM team can create code that can work for specific projects and can  also help make your work  process faster</p>
                                <ul>
                                    <li>Statistics calculation</li>
                                    <li>Report Management</li>
                                    <li>Transaction Management</li>
                                    <li>Transaction Processing</li>
                                    <li>Connection to server system information service.</li>
                                </ul>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion-group" style='background-color: #99cc67;'>
                <div class="accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse4">
                        <i class='icon-trycatch mobile'></i>Mobile Applications
                    </a>
                </div>
                <div id="collapse4" class="accordion-body collapse">
                    <div class="accordion-inner clearfix">
                        <div class="grid_3 begin">
                            <div class="grid_3_mockup"><img src="<?php bloginfo('template_directory'); ?>/img/bot_mobile_2.png" alt="Mobile Application" ></div>
                        </div>
                        <div class="detail-box">  
                            <?php if (qtrans_getLanguage() == "th"): ?>
                                <p>โปรแกรมสำหรับอุปกรณ์มือถือ</p>
                                <p>เรา <a href="<?php echo home_url(); ?>">TryCatch&trade;</a> รับทำโปรแกรมแอพลิเคชั่นมือถือ Android ที่สามารถตอบสนองกับประภทงานที่คุณต้องการ ไม่ว่าจะเป็น</p>
                                <ul>
                                    <li>งานประมวลผลที่จบภายในตัว</li>
                                    <li>แอพลิเคชั่นที่ทำงานเชื่อมต่อกับระบบ Server บริการข้อมูล</li>
                                    <li>แอพลิเคชั่นการสั่งการผ่านระบบเครือข่ายภายใน</li>
                                    <li>อื่นๆ ลองติดต่อมาคุยกับทางเรา</li>
                                </ul>
                            <?php else: ?>
                                <p>Programs for mobile devices</p>
                                <p>Our TrycatchTM team makes program applications that are compatible with Android and can interact with any kind of work.</p>
                                <ul>
                                    <li>End-Process work</li>
                                    <li>Applications that work and connect with server system service information.</li>
                                    <li>Applications that can command inside networks</li>
                                    <li>Contact us for more information!</li>
                                </ul>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion-group" style='background-color: #9898ca;'>
                <div class="accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse5">
                        <i class='icon-trycatch social-ball'></i>Network Solutions
                    </a>
                </div>
                <div id="collapse5" class="accordion-body collapse">
                    <div class="accordion-inner clearfix">
                        <div class="grid_3 begin">
                            <div class="grid_3_mockup"><img src="<?php bloginfo('template_directory'); ?>/img/bot_network_2.png" alt="Network Solution" ></div>
                        </div>
                        <div class="detail-box">                                 
                            <?php if (qtrans_getLanguage() == "th"): ?>
                                <p>ระบบเครือข่ายภายในอาคาร</p>
                                <p>เรา <a href="<?php echo home_url(); ?>">TryCatch&trade;</a> สามารถทำระบบ Network Solution ให้กับทาง Office ขนาดเล็กถึงขนาดกลาง โดยระบบของทาง เรา <a href="<?php echo home_url(); ?>">TryCatch&trade;</a> มีความสามารถในการบริหาร</p>
                                <ul>
                                    <li>การจัดการบริหารจัดการ Internet ภายในองค์กร</li>
                                    <li>การ Block เว็บไม่พึงประสงค์</li>
                                    <li>การจัดการระบบ File Sharing</li>
                                    <li>การจัดการ File Sharing แบบ Online</li>
                                    <li>อื่นๆ ลองมาปรึกษากับทาง <a href="<?php echo home_url(); ?>">TryCatch&trade;</a> ได้ครับ</li>
                                </ul>
                            <?php else: ?>
                                <p>For in-house network systems</p>
                                <p>Our TrycatchTM  team can make systems for Network Solutions for small and middle-sized offices.Our system is  capable of manage.</p>
                                <ul>
                                    <li>Managing the internet inside the organization (filters,etc.)</li>
                                    <li>Blocking Spam and malware.</li>
                                    <li>Managing file-sharing systems.</li>
                                    <li>Managing file-sharing online.</li>
                                    <li>Other measures TrycatchTM can advise you on.</li>
                                </ul>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="accordion-group" style='background-color: #989898;'>
                <div class="accordion-heading">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse6">
                        <i class='icon-trycatch comment'></i>Another solution?,Let’s Discuss.
                    </a>
                </div>
                <div id="collapse6" class="accordion-body collapse">
                    <div class="accordion-inner clearfix">
                        <div class="grid_3 begin">
                            <div class="grid_3_mockup"><img src="<?php bloginfo('template_directory'); ?>/img/bot_discuss_2.png" alt="Another Solution?, Let's Descuss!" ></div>
                        </div>
                        <div class="detail-box">     
                            <?php if (qtrans_getLanguage() == "th"): ?>
                                <p>สำหรับ Project อื่นๆ</p>
                                <p>สามารถเข้ามาปรึกษา กับเราได้โดยไม่มีค่าใช้จ่ายใดๆ ครับ หรือสามารถส่งข้อความหา <a href="<?php echo home_url(); ?>">TryCatch&trade;</a> ได้ที่หน้า <a href='/th/contact-us/'>Contact Us</a> ได้เลยครับ ^^</p>
                            <?php else: ?>
                                <p>For  Other Projects</p>
                                <p>You can consult with us for free or you can send a message through our  Contact Us page  immediately !!! Thank you ^^ </p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!--End According bar-->
    </div>
    <!--Get Sidebar-->
    <?php get_sidebar(); ?>
    <!--End Sidebar-->

</div><!-- end main content -->

<script type="text/javascript">
    $(function() {
        $('.connect-us').first().css({'z-index': 2, 'opacity': 1});
        var total = $('.connect-us').length;

        var current_ele, next_ele, index = 0;
        current_ele = $('.connect-us').first();

        setInterval(function() {
            if (index < (total - 1)) {
                next_ele = $(current_ele).next();
                index++;
            } else {
                next_ele = $('.connect-us').first();
                index = 0;
            }

//            console.log("Index : " + index);

            $(next_ele).css({'opacity': 0, 'z-index': 1});
            var im = $(current_ele).find('.grid_3_mockup');
            var txt = $(current_ele).find('.grid_6');

            $(im).animate({'margin-left': 230}, 250, function() {
                $(txt).animate({opacity: 0}, 250, function() {
                    var im_next = $(next_ele).find('.grid_3_mockup');
                    var txt_next = $(next_ele).find('.grid_6');

                    $(im_next).css({opacity: 1, 'margin-left': 230});
                    $(txt_next).css({opacity: 0});

                    $(current_ele).css({opacity: 0, 'z-index': 0});
                    $(next_ele).css({opacity: 1, 'z-index': 2});

                    $(im_next).animate({'margin-left': 0}, 250, function() {
                        $(txt_next).animate({'opacity': 1}, 250);
                        current_ele = next_ele;
                    });
                });
            });
        }, 10000);
    });
</script>

<?php get_footer(); ?>