
<!-- footer-freeform -->
<div class="footer-freeform">
    <div class="container_12">
        <div class="clearfix footer-section-box">
            <div class="grid_4">
                <h3 class="block_title"><font class="textblue">A</font>bout Trycatch&trade;</h3>
                <p style="height: 50px; margin-top: -10px;">
                    <i class="footer-icon pin"></i>
                    <span class="address">Address<br/>122/99 M.2 Donkaew Maerim Chiang Mai Thailand 50180</span>
                </p>
                <p style="height: 32px;">
                    <i class="footer-icon tel"></i>
                    <span class="tel_txt">+66 83 323 9475<br/>+66 89 700 2217</span>
                </p>
                <p style="height: 30px;">
                    <i class="footer-icon mail"></i>
                    <span class="contact_txt"><a href="mailto:keittirat@trycatch.in.th">keittirat@trycatch.in.th</a></span>
                </p>
                <h3 class="block_title" style="margin-top: -12px;"><font class="textblue">K</font>eep in touch</h3>
            </div>
            <div class="grid_4">
                <h3 class="block_title"><font class="textblue">R</font>eference feed</h3>
                <?php $feed = get_feed(); ?>
                <ul class="smash_feed_link">
                    <?php for ($i = 0; $i < 5; $i++): ?>
                        <?php $record = $feed[$i]; ?>
                        <li>
                            <a href="<?php echo $record['link'] ?>" target="_blank"><?php echo $record['title'] ?></a>
                        </li>
                    <?php endfor; ?>
                </ul>
            </div>
            <div class="grid_4">
                <h3 class="block_title"><font class="textblue">Q</font>uick Contact</h3>
                <form class="contact-form-quick" method="post" action="<?php echo (qtrans_getLanguage() == "th") ? "/th/contact-us/" : "/contact-us/"; ?>">
                    <input type="text" name="contact-name" placeholder="Name">
                    <input type="text" name="contact-mail" placeholder="E-mail">
                    <textarea name="contact-detail" placeholder="Message"></textarea>
                    <div class="clearfix">
                        <button style="float: right;" type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    <input type="hidden" value="http://<?php echo $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]; ?>" id="destination">
                </form>
            </div>
        </div>
        <div class="footer-ability-box">
            <div class="clearfix foot-icon">
                <img src="<?php bloginfo('template_directory'); ?>/img/compatabillity_icon.png">
                <span>This website is work great on all devices<br/>Powered by TryCatch&trade; <?php echo (date('Y') == 2013) ? "2013" : "2013-" . date('Y'); ?></span>
            </div>
        </div>
    </div>
</div><!-- end footer-freeform -->

<div class="clear">
    <?php echo qtrans_generateLanguageSelectCode('both'); ?>
</div>

<?php wp_footer(); ?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=196963150420944";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-42013443-3', 'trycatch.in.th');
    ga('send', 'pageview');

</script>
</body>
</html>
