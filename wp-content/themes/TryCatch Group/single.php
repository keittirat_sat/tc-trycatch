<?php get_header(); ?>
<div class="mini-slide-freeform">
    <div class="container_12" style="position: relative; height: 150px;">
        <h1 class="cate_title ">            
            <font class="textblue">Port</font>folio.
        </h1>
    </div>
</div>

<div class="container_12 main-content">
    <div class="grid_9">    
        <!--Category Bar-->
        <?php the_post(); ?>
        <h2 class="textblue subtitle-forcate " style="background: none;"><?php the_title(); ?></h2>
        <?php $cate = wp_get_post_categories(get_the_ID()); ?>
        <!--Breadcrumb-->
        <ul class="breadcrumb">            
            <li><a href="<?php echo get_page_link(18) ?>"><?php echo (qtrans_getLanguage() == "th") ? "ผลงาน" : "Portfolio"; ?></a> <span class="divider">&raquo;</span></li>
            <li>
                <?php $i = 0; ?>
                <?php foreach ($cate as $rec): ?>
                    <?php $c = get_category($rec); ?>
                    <?php $c->link = get_category_link($rec); ?>
                    <?php if ($i != 0): ?>
                        <span class="divider">,</span>
                    <?php endif; ?>
                    <?php $i++; ?>
                    <a href="<?php echo $c->link; ?>"><?php echo $c->name; ?></a>
                <?php endforeach; ?>
                <span class="divider">&raquo;</span>
            </li>
            <li class="active"><?php the_title(); ?></li>
        </ul>
        <!--End Breadcrumb-->

        <p><!--Begin Thumbnail IMG--><?php echo get_the_post_thumbnail(); ?><!--End Thumbnail IMG--></p>
        <p><?php the_excerpt(); ?></p>
        <?php $url = get_post_custom_values('url'); ?>
        <?php if (count($url)): ?>
            <p>
                <a href="<?php echo $url[0]; ?>" target="_blank" rel="nofollow"><i class="icon-trycatch cate-link"></i>Go to Client's site</a>
            </p>
        <?php endif; ?>
        <p><?php the_content(); ?></p>
        <p class='clearfix'>
            <?php $image = get_all_post_image(get_the_ID(), 'post-thumbnails'); ?>
            <?php foreach ($image as $img): ?>
                <a href="<?php echo $img ?>" style="background-image:url('<?php echo $img ?>');" class="grid_1 begin portfolio-other-thumb colorbox" rel="port_<?php echo get_the_ID(); ?>" title="<?php echo get_the_title(); ?>"></a>
            <?php endforeach; ?>
        </p>


        <h2 class=" relate-post-label"><font class="textblue">Relate</font> works</h2>

        <?php $relate = get_relate(3); ?>
        <?php $i = 0; ?>
        <?php foreach ($relate as $rec): ?>
            <div class="grid_3 <?php echo ($i == 0) ? "begin" : "" ?>  <?php echo ($i == 2) ? "end" : "" ?>">
                <a class="post_relate" style="background-image:url('<?php echo $rec['img']['medium'][0]; ?>'); " href="<?php echo $rec['link']; ?>">

                </a>
                <p class="relate-post-title">
                    <a href="<?php echo $rec['link'] ?>" class=""><?php echo $rec['title']; ?></a>
                </p>
            </div>
            <?php $i++; ?>
        <?php endforeach; ?>

    </div>
    <?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>